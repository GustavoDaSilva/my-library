import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterBook } from 'src/app/model/book.model';

@Component({
    selector: 'app-register-book',
    templateUrl: './register-book.component.html',
    styleUrls: ['./register-book.component.css']
})
export class RegisterBookComponent {

    formBook = this.formBuilder.group({
        title: ['', [ Validators.required ] ],
        author: ['', [ Validators.required ] ],
        publisher: ['', [ Validators.required ] ],
        summary: ['', [ Validators.required ] ],
        edition: ['', [ Validators.required ] ],
        isbn: ['', [ Validators.required ] ],
        publicationYear: ['', [ Validators.required ] ],
        pages: ['', [ Validators.required ] ]
    })

    constructor(private formBuilder: FormBuilder,
                private router: Router) { }

    save() {
        let book = new RegisterBook(
            this.title.value,
            this.author.value,
            this.edition.value,
            this.publicationYear.value,
            this.summary.value,
            this.publisher.value,
            this.pages.value,
            this.isbn.value
        )        
    }

    cancel() {
        this.router.navigateByUrl("/")
    }

    get title() {
        return this.formBook.controls.title
    }

    get author() {
        return this.formBook.controls.author
    }

    get publisher() {
        return this.formBook.controls.publisher
    }

    get summary() {
        return this.formBook.controls.summary
    }

    get isbn() {
        return this.formBook.controls.isbn
    }

    get edition() {
        return this.formBook.controls.edition
    }

    get publicationYear() {
        return this.formBook.controls.publicationYear
    }

    get pages() {
        return this.formBook.controls.pages
    }
}
