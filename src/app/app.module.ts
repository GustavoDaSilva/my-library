import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './ui/home/app.component';
import { ListBooksComponent } from './ui/list-books/list-books.component';
import { RegisterBookComponent } from './ui/register-book/register-book.component';
import { DetailsBookComponent } from './ui/details-book/details-book.component';

@NgModule({
  declarations: [
    AppComponent,
    ListBooksComponent,
    RegisterBookComponent,
    DetailsBookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
